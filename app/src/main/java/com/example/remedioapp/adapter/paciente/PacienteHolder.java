package com.example.remedioapp.adapter.paciente;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.remedioapp.R;

public class PacienteHolder extends RecyclerView.ViewHolder {

    private TextView nomePaciente, qtdRemedio;
    private ImageView imgSexo;

    public PacienteHolder(@NonNull View itemView) {
        super(itemView);
        nomePaciente = itemView.findViewById(R.id.tvListagemNomePaciente);
        qtdRemedio = itemView.findViewById(R.id.tvListagemQtdRemedios);
        imgSexo = itemView.findViewById(R.id.ivListagemSexoPaciente);
    }

    public TextView getNomePaciente() {
        return nomePaciente;
    }

    public TextView getQtdRemedio() {
        return qtdRemedio;
    }

    public ImageView getImgSexo() {
        return imgSexo;
    }
}
