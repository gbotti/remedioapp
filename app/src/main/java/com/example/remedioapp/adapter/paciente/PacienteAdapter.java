package com.example.remedioapp.adapter.paciente;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.remedioapp.R;
import com.example.remedioapp.model.ESexo;
import com.example.remedioapp.model.Paciente;

import java.util.ArrayList;

public class PacienteAdapter extends RecyclerView.Adapter<PacienteHolder>{

    private ArrayList<Paciente> pacientes;

    public PacienteAdapter(ArrayList<Paciente> pacientes){
        this.pacientes = pacientes;
    }

    @NonNull
    @Override
    public PacienteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PacienteHolder(LayoutInflater.from( parent.getContext())
                .inflate(R.layout.linha_paciente,parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PacienteHolder holder, int position) {

        holder.getNomePaciente().setText( pacientes.get( position ).getNome());
        holder.getQtdRemedio().setText("Quantidade de Remédios: "
                +pacientes.get( position ).getRemedios().size());

        if (pacientes.get( position ).getSexo() == ESexo.Feminino){
            holder.getImgSexo().setImageResource(R.drawable.gender_woman_icon);
        }
    }

    @Override
    public int getItemCount() {
        return pacientes.size();
    }
}
