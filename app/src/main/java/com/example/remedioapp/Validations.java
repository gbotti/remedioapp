package com.example.remedioapp;

import com.google.android.material.textfield.TextInputLayout;

public class Validations {

    public static boolean validaCampoVazio(TextInputLayout campo) {
        String valor = campo.getEditText().getText().toString().trim();

        if (valor.isEmpty()){
            campo.setError("campo não pode estar vazio");
            //campo.requestFocus();
            return false;
        }else{
            campo.setError(null);
            return true;
        }
    }

    public static boolean validaCampoTamanho(TextInputLayout campo) {
        String valor = campo.getEditText().getText().toString();

        if (valor.length() > campo.getCounterMaxLength()){
            campo.setError("tamanho máximo excedido");
            //campo.requestFocus();
            return false;
        }else{
            campo.setError(null);
            return true;
        }
    }
}
