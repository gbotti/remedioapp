package com.example.remedioapp.model;


import java.io.Serializable;
import java.util.Date;

public class Remedio implements Serializable {

    private String nome, observacao;
    private int vezesDia;
    private Date dataRemedio;
    private Paciente paciente;

    public Remedio(String nome, String observacao, int vezesDia, Date dataRemedio, Paciente paciente) {
        this.nome = nome;
        this.observacao = observacao;
        this.vezesDia = vezesDia;
        this.dataRemedio = dataRemedio;
        this.paciente = paciente;
    }

    public Remedio() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public int getVezesDia() {
        return vezesDia;
    }

    public void setVezesDia(int vezesDia) {
        this.vezesDia = vezesDia;
    }

    public Date getDataRemedio() {
        return dataRemedio;
    }

    public void setDataRemedio(Date dataRemedio) {
        this.dataRemedio = dataRemedio;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
