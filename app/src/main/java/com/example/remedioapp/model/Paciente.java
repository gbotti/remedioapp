package com.example.remedioapp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Paciente implements Serializable {
    private String nome, endereco, obs;
    private boolean ehFumante, ehHipertenso, ehDiabetico;
    private ESexo sexo;
    private ArrayList<Remedio> remedios;

    public Paciente() {
        remedios = new ArrayList<>();
    }

    public Paciente(String nome, String endereco, String obs, boolean ehFumante, boolean ehHipertenso, boolean ehDiabetico, ESexo sexo) {
        this.nome = nome;
        this.endereco = endereco;
        this.obs = obs;
        this.ehFumante = ehFumante;
        this.ehHipertenso = ehHipertenso;
        this.ehDiabetico = ehDiabetico;
        this.sexo = sexo;
        remedios = new ArrayList<>();
    }

    @Override
    public String toString() {
        return nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paciente paciente = (Paciente) o;
        return nome.equals(paciente.nome) && endereco.equals(paciente.endereco);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nome, endereco);
    }

    public String getNome() {
        return nome;
    }

    public ArrayList<Remedio> getRemedios() {
        return remedios;
    }

    public void setRemedios(ArrayList<Remedio> remedios) {
        this.remedios = remedios;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public boolean isEhFumante() {
        return ehFumante;
    }

    public void setEhFumante(boolean ehFumante) {
        this.ehFumante = ehFumante;
    }

    public boolean isEhHipertenso() {
        return ehHipertenso;
    }

    public void setEhHipertenso(boolean ehHipertenso) {
        this.ehHipertenso = ehHipertenso;
    }

    public boolean isEhDiabetico() {
        return ehDiabetico;
    }

    public void setEhDiabetico(boolean ehDiabetico) {
        this.ehDiabetico = ehDiabetico;
    }

    public ESexo getSexo() {
        return sexo;
    }

    public void setSexo(ESexo sexo) {
        this.sexo = sexo;
    }
}
