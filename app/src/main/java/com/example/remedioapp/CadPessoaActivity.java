package com.example.remedioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.remedioapp.model.ESexo;
import com.example.remedioapp.model.Paciente;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class CadPessoaActivity extends AppCompatActivity {

    private TextInputLayout cpNome, cpEndereco, cpObs;
    private CheckBox cpFumante, cpHipertenso, cpDiabetico;
    private Button btSalvar;
    private RadioButton cpMasc, cpFem;
    private RadioGroup cpSexo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_pessoa);

        binding();



        btSalvar.setOnClickListener(cliqueSalvar());
    }

    private View.OnClickListener cliqueSalvar() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!Validations.validaCampoVazio(cpNome) || !Validations.validaCampoTamanho(cpNome)
                        || !Validations.validaCampoVazio(cpEndereco) || !Validations.validaCampoTamanho(cpEndereco)){
                    return;
                }

                Paciente pac = new Paciente(cpNome.getEditText().getText().toString(),
                        cpEndereco.getEditText().getText().toString(),
                        cpObs.getEditText().getText().toString(),
                        cpFumante.isChecked(),
                        cpDiabetico.isChecked(),
                        cpHipertenso.isChecked(),
                        cpMasc.isChecked()? ESexo.Masculino : ESexo.Feminino);
                Intent i = new Intent();
                i.putExtra("paciente", pac);
                setResult(10, i);

                finish();

                //Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_LONG).show();
            }
        };
    }


    private void binding() {
        cpNome = findViewById(R.id.tilNome);
        cpEndereco = findViewById(R.id.tilEndereco);
        btSalvar = findViewById(R.id.btnSalvar);
        cpObs = findViewById(R.id.tilObs);
        cpFumante = findViewById(R.id.cbFumante);
        cpDiabetico = findViewById(R.id.cbDiabetico);
        cpHipertenso = findViewById(R.id.cbHipertenso);
        cpMasc = findViewById(R.id.rMasc);
    }
}