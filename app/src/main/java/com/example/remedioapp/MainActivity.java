package com.example.remedioapp;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.remedioapp.model.ESexo;
import com.example.remedioapp.model.Paciente;
import com.example.remedioapp.model.Remedio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnCadastroPessoa;
    Button btnRemedio;
    Button btnListar;
    TextView lblInfo;

    private ArrayList<Paciente> pacientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding();

        preparedDatabase();

        registraEventos();
    }

    private void preparedDatabase() {
        pacientes = new ArrayList<>();
        pacientes.add ( new Paciente("Ze","rua x","",false,true,false, ESexo.Masculino));
        pacientes.add ( new Paciente("Botti","rua x2","",true,false,false, ESexo.Masculino));
        pacientes.add ( new Paciente("Ana","rua x3332","1246890*",true,false,true, ESexo.Feminino));

        pacientes.get(0).getRemedios().add( new Remedio("Gadernal", "5 vezes ao dia", 5, new Date(), null ));
        lblInfo.setText("Total de Pacientes: "+pacientes.size());
    }

    private void registraEventos() {

        btnCadastroPessoa.setOnClickListener( callViewCadastroPessoa() );

        btnRemedio.setOnClickListener( callViewCadastroRemedio() );

        btnListar.setOnClickListener( callViewListagem() );

    }

    private View.OnClickListener callViewListagem() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), ListagemActivity.class);

                itn.putExtra("pacientes", pacientes);

                startActivity(itn);
            }
        };
    }

    private View.OnClickListener callViewCadastroRemedio() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), CadRemedioActivity.class);

                itn.putExtra("pacientes", pacientes);

                //startActivity(itn);
                viewCadastroRemedio.launch(itn);
            }
        };
    }

    ActivityResultLauncher<Intent> viewCadastroPaciente = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == 10) {

                        Paciente p = (Paciente) result.getData().getExtras().getSerializable("paciente");

                        pacientes.add(p);

                        lblInfo.setText("Total de Pacientes: "+pacientes.size());

                        Toast.makeText(getApplicationContext(), "Paciente "+p.getNome()+ "cadastrado com suceso", Toast.LENGTH_LONG).show();
                    }
                }
            }
    );

    ActivityResultLauncher<Intent> viewCadastroRemedio = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {

                    if (result.getResultCode() == 10){

                        Remedio r = (Remedio) result.getData().getExtras().getSerializable("remedio");

                        int idx = pacientes.indexOf(r.getPaciente());

                        pacientes.get(idx).getRemedios().add(r);

                        Toast.makeText(getApplicationContext(), r.getPaciente().getNome()+" toma o remédio "+r.getNome(),
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
    );

    private View.OnClickListener callViewCadastroPessoa() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), CadPessoaActivity.class);

                //startActivity(itn);
                viewCadastroPaciente.launch(itn);
            }
        };
    }

    private void binding() {

        btnCadastroPessoa = findViewById(R.id.btnCadPessoa);
        btnRemedio = findViewById(R.id.btnCadRemedio);
        btnListar = findViewById(R.id.btnListagem);
        lblInfo = findViewById(R.id.tvTotalPacientes);
    }
}