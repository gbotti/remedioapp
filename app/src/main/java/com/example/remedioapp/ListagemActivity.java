package com.example.remedioapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.remedioapp.adapter.paciente.PacienteAdapter;
import com.example.remedioapp.model.Paciente;

import java.util.ArrayList;

public class ListagemActivity extends AppCompatActivity {

    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        binding();

        ArrayList<Paciente> pacientes = (ArrayList<Paciente>) getIntent().getExtras().getSerializable("pacientes");

        PacienteAdapter PacAdp = new PacienteAdapter( pacientes );

        LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager( layout );

        rv.setAdapter( PacAdp );
    }

    private void binding() {
        rv = findViewById(R.id.rvListagem);
    }
}