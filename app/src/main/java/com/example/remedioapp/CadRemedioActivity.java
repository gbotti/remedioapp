package com.example.remedioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.remedioapp.model.Paciente;
import com.example.remedioapp.model.Remedio;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class CadRemedioActivity extends AppCompatActivity {
    
    Spinner cpPaciente;
    Button btn;
    TextInputLayout cpNome, cpHorario, cpQtd, cpObs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_remedio);

        binding();

        ArrayList<Paciente> pacientes = (ArrayList<Paciente>) getIntent().getExtras().getSerializable("pacientes");
        
        preencheSpinner(pacientes);
        
        btn.setOnClickListener( saveRemedioClick());

    }

    private View.OnClickListener saveRemedioClick() {

        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("aviso", "Data: "+cpHorario.getEditText().getText().toString());
                Log.i("aviso", "ItemID: "+cpPaciente.getSelectedItemId());
                Log.i("aviso", "ID: "+cpPaciente.getSelectedItemPosition());

                ArrayList<Paciente> pacientes = (ArrayList<Paciente>) getIntent().getExtras().getSerializable("pacientes");

                Paciente p = pacientes.get( cpPaciente.getSelectedItemPosition() );

                Remedio red = new Remedio( cpNome.getEditText().getText().toString(),
                        cpObs.getEditText().getText().toString(),
                        Integer.parseInt( cpQtd.getEditText().getText().toString() ),
                        null,
                        (Paciente) cpPaciente.getSelectedItem()
                );

                Intent i = new Intent();

                i.putExtra("remedio", red);

                setResult(10, i);

                finish();
            }
        };
    }

    private void preencheSpinner(ArrayList<Paciente> pacientes) {

        ArrayList<String> lista = new ArrayList<>();
        for (Paciente p : pacientes) {
            lista.add(p.getNome());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1, lista);

        ArrayAdapter<Paciente> adapter1 = new ArrayAdapter<Paciente>(getApplicationContext(),
                android.R.layout.simple_list_item_1, pacientes);

        cpPaciente.setAdapter(adapter1);
    }

    private void binding() {
        cpPaciente = findViewById(R.id.spPaciente);
        cpNome = findViewById(R.id.tilNomeRemedio);
        cpHorario = findViewById(R.id.tilHorario);
        cpQtd = findViewById(R.id.tilQuantidade);
        cpObs = findViewById(R.id.tilObsRemedio);
        btn = findViewById(R.id.btnSalvarRemedio);
    }
}